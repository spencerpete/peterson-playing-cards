#include <iostream>
#include <conio.h>

using namespace std;

enum Rank : int
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE,
	JOKER = 10,

};

enum Suit
{
	DIAMOND,
	CLUB,
	SPADE,
	HEART
};

struct Card
{
	Rank rank;
	Suit suit;
};


int main()
{
	return 0;
}